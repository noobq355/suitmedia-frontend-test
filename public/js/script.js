// header
var prevScrollPos = window.scrollY;
var header = document.getElementById("main-header");

window.onscroll = function () {
  var currentScrollPos = window.scrollY;

  if (currentScrollPos === 0) {
    header.style.top = "0";
    header.classList.remove("transparent");
  } else if (prevScrollPos > currentScrollPos) {
    header.style.top = "0";
    header.classList.add("transparent");
  } else {
    header.style.top = "-120px";
    header.classList.remove("transparent");
  }

  prevScrollPos = currentScrollPos;
};

// parallax
document.addEventListener("DOMContentLoaded", function () {
  var parallaxContainer = document.querySelector(".hero-container");
  var parallaxBg = document.querySelector(".parallax");

  function updateParallax() {
    var scrolled = window.scrollY;
    parallaxBg.style.transform = "translateY(" + scrolled * 0.4 + "px)";
  }

  window.addEventListener("scroll", updateParallax);
});

// pagination
var pages = document.querySelectorAll(".page");

pages.forEach(function (page) {
  page.addEventListener("click", function () {
    pages.forEach(function (p) {
      p.classList.remove("active");
    });

    this.classList.add("active");
  });
});

// hamburger
document.addEventListener("DOMContentLoaded", function () {
  const mobileMenuButton = document.getElementById("mobile-menu");
  const navMenu = document.getElementById("nav-menu");

  mobileMenuButton.addEventListener("click", function () {
    navMenu.classList.toggle("active");
  });
});
