const express = require("express");
const routes = require("./routes");
const cors = require("cors");

const app = express();
const PORT = 8080;

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.static("public"));

app.use(routes);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(PORT, () => {
  console.log(`Server running on port: http://localhost:${PORT}/`);
});
