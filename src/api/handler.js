const axios = require("axios");

exports.ideas = async (req, res) => {
  try {
    const apiUrl = "https://suitmedia-backend.suitdev.com/api/ideas";
    const response = await axios.get(apiUrl, { params: req.query });
    res.json(response.data);
  } catch (error) {
    console.log("Proxy error:", error.message);
    res.status(500).json({ error: error.message });
  }
};
