const express = require("express");

const { ideas } = require("./api/handler");

const routes = express.Router();

routes.get("/", (req, res) => {
  res.redirect("/pages/index.html");
});

routes.get("/api/ideas", ideas);

module.exports = routes;
